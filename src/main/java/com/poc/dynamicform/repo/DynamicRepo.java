package com.poc.dynamicform.repo;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.poc.dynamicform.entity.DynamicForm;

@Repository
public interface DynamicRepo extends MongoRepository<DynamicForm,Long>{

	DynamicForm findByName(String name);
	
	
}
