package com.poc.dynamicform.serviceimpl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.poc.dynamicform.entity.DynamicForm;
import com.poc.dynamicform.repo.DynamicRepo;
import com.poc.dynamicform.service.DynamicService;

@Service
public class DynamicServiceImpl implements DynamicService{

	@Autowired
	private DynamicRepo repo;
	
	@Override
	public DynamicForm getDynamicForm(String name) {
	return repo.findByName(name);
	}

	@Override
	public void saveForm(DynamicForm dynamicForm) {
		repo.save(dynamicForm);
		
	}

	


	
	
	

	
	
}
