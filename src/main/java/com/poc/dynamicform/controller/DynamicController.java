package com.poc.dynamicform.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.poc.dynamicform.entity.DynamicForm;
import com.poc.dynamicform.service.DynamicService;

@RestController
public class DynamicController {

	@Autowired
	private DynamicService service;

	/*	//Hard Code data 
	@PostMapping("/dynamicform/create")
	public void createData() {
		

	WidgetData widgetData11=new WidgetData(1L,true,"tittle1","value1");
	WidgetData widgetData12=new WidgetData(1L,false,"tittle2","value2");
	
	List<WidgetData> w1=new ArrayList<>();
	
    
		w1.add(widgetData11);
		w1.add(widgetData12);
		
	Fields field1= new Fields(200L,"Name","Title",Type.EDIT_TEXT,10,1,false,null);
	Fields field2= new Fields(200L,"Number","Phone Number",Type.EDIT_TEXT_PHONE,10,1,false,null);
	Fields field3= new Fields(200L,"Insurance Image","Title",Type.IMAGE_PICKER,10,1,false,null);
	Fields field4= new Fields(200L,"Radio","Radio Button",Type.RADIO,10,1,false,w1);
	
	List<Fields> f1= new ArrayList<>();
		
	f1.add(field1);
	f1.add(field2);
	f1.add(field3);
	f1.add(field4);
	
	
	DynamicForm r1= new DynamicForm(10001L,"Receptionist Form",f1);
	
	repo.save(r1);
	}
*/
	@PostMapping("/dynamicform/createform")
	public void createForm(@RequestBody DynamicForm dynamicForm) {

		service.saveForm(dynamicForm);
	}

	@GetMapping("/dynamicform/get/{name}")
	public DynamicForm getData(@PathVariable String name) {

		return service.getDynamicForm(name);

	}

}
