package com.poc.dynamicform.entity;

import java.util.List;

import com.poc.dynamicform.enums.Type;


public class Fields {
	

	private String id;
	
	private String name;
	
	private String title;
	
	private Type type;
	
	private int maxLength;
	
	private int minLength;
	
	private boolean isChecked;
	
	private List<WidgetData> widgetData;
	
	public Fields() {
		super();
	}

	

	
	



	public Fields(String id, String name, String title, Type type, int maxLength, int minLength, boolean isChecked,
			List<WidgetData> widgetData) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.type = type;
		this.maxLength = maxLength;
		this.minLength = minLength;
		this.isChecked = isChecked;
		this.widgetData = widgetData;
	}








	public List<WidgetData> getWidgetData() {
		return widgetData;
	}




	public void setWidgetData(List<WidgetData> widgetData) {
		this.widgetData = widgetData;
	}





	public String getId() {
		return id;
	}








	public void setId(String id) {
		this.id = id;
	}








	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public int getMinLength() {
		return minLength;
	}

	public void setMinLength(int minLength) {
		this.minLength = minLength;
	}

	public boolean isChecked() {
		return isChecked;
	}

	public void setChecked(boolean isChecked) {
		this.isChecked = isChecked;
	}

	
	
	
}
