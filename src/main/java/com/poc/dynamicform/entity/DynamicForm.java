package com.poc.dynamicform.entity;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class DynamicForm {

	
	private String _id;
	
	private String name;
	
	private List<Fields> field;
	
	public DynamicForm() {
		super();
	}
	

	public DynamicForm(String _id, String name, List<Fields> field) {
		super();
		this._id = _id;
		this.name = name;
		this.field = field;
	}






	

	public String get_id() {
		return _id;
	}






	public void set_id(String _id) {
		this._id = _id;
	}






	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Fields> getField() {
		return field;
	}

	public void setField(List<Fields> field) {
		this.field = field;
	}


	
	
	
	
}
