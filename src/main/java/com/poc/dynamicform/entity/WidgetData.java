package com.poc.dynamicform.entity;

public class WidgetData {

	
	private String id;
	private boolean isSelected;

	private String title;
	private String value;

	public WidgetData() {
		super();
		// TODO Auto-generated constructor stub
	}

	

	public WidgetData(String id, boolean isSelected, String title, String value) {
		super();
		this.id = id;
		this.isSelected = isSelected;
		this.title = title;
		this.value = value;
	}


	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public boolean isSelected() {
		return isSelected;
	}

	public void setSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
