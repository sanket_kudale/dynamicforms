package com.poc.dynamicform.enums;

public enum Type {

	IMAGE_PICKER, EDIT_TEXT_PHONE, EDIT_TEXT, LABEL, RADIO, CHECKBOX;
}
