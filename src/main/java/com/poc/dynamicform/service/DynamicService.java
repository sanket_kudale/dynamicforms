package com.poc.dynamicform.service;

import com.poc.dynamicform.entity.DynamicForm;

public interface DynamicService {

	DynamicForm getDynamicForm(String name);
	
	void saveForm(DynamicForm dynamicForm);

}
